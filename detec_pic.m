function [peak_pos,Th]=detec_pic(x,alfa,M)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PEAK_VECT
% peak_pos=detec_pic(x,alfa,M)
%
% Una vez sustra�da la media de la se�al, se detectan los picos de m�xima 
% amplitud positiva de una se�al, que superan un umbral Th (Th= alfa*Amax,
% siendo alfa un coeficiente introducido por el usuario y Amax el valor
% m�ximo amplitud de la se�al). Adem�s cada pico ha de tener mayor valor que
% las M muestras de su izquierda y de su derecha. Tambien se pueden localizar
% picos en los puntos del comienzo y final de la se�al. En este caso, se 
% vigilar� que no haya muestras con mayor valor en un entorno de al menos 
% M muestras alrededor del pico detectado.
% 
% x: se�al de entrada
% alfa: coeficiente para determinar el umbral
% M: numero de muestras que debe superar cada pico a su izquierda y a su derecha.
% peak_pos: vector de posiciones de los picos detectados
% Th: valor umbral resultante
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

L= length(x);
x= x-mean(x);
Amax= max(x);
Th= alfa*Amax;
peak_pos= [];
for n=1: M
    if (x(n)>=Th) && (x(n)>= max([x(1:n-1),x(n+1:n+M)]))
        peak_pos= [peak_pos, n];
    end
end
for n= M+1: L-M
    if (x(n)>=Th) && (x(n)>= max(x(n-M:n-1))) && (x(n)> max(x(n+1:n+M)))
        peak_pos= [peak_pos, n];
    end
end
for n=L-M+1: L-1
    %if ((x(n)>=Th) && (x(n)>= max([x(n-M:n-1),x(n+1:L)])))
   if x(n)>= Th
       if x(n) >= max(x(n-M:L))
            peak_pos= [peak_pos, n];
       end
    end
end

figure, plot(x), grid
hold on, plot(peak_pos,x(peak_pos),'ro')
plot(Th*ones(1,L),'r--')
