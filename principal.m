clear all;
close all; 
 
whos -file ecgConditioningExample.mat; 
load('ecgConditioningExample.mat','ecg')   
fs=1000;  
Ts=1/fs;
t = (0:Ts:29.999);
a=1;b = [1,-1]; b4=[1/8,1/8,1/8,1/8,1/8,1/8,1/8,1/8]; b3=[1,0,-1];
radio=0.9;b2=[1,-2*cos(0.1*pi),1];  a2=[1,-1.8*cos(0.1*pi),0.81]; s=1;
no_conectado=[];

% Representacion temporal 
figure,subplot(2,3,1),plot(t,ecg(:,1)),title('ECG 1')
subplot(2,3,2),plot(t,ecg(:,2)),title('ECG 2')
subplot(2,3,3),plot(t,ecg(:,3)),title('ECG 3')
subplot(2,3,4),plot(t,ecg(:,4)),title('ECG 4')    
subplot(2,3,5),plot(t,ecg(:,5)),title('ECG 5')
subplot(2,3,6),plot(t,ecg(:,6)),title('ECG 6')

plot_mult_curves2(t,ecg,10000^2);title('Representacion temporal de los 6 canales')

% Podemos ver que el canal 4 no está conectado; los canales 3 y 6 tiene picos de gran amplitud.
 
N=width(ecg);
% Loop para realizar el preprocesamiento a los 6 canales (i=1,2,..,6)
for i=1:N
    mi_senal=ecg(:,i);
   
    %Deteccion de canal no conectado. Guarda el número del canal en el vector 'no_conectado'
    media=mean(mi_senal);
    if media==0
        no_conectado(s)=i;
        s=s+1;
    end
     
    if media~=0     %si el canal esta conectado comienza el preprocesamiento 
        % PASO 1) Eliminación de picos de registro (artefactos de gran amplitud)
        % Los pasos a seguir para la deteccion y eliminación de picos son los siguientes: derivación, cuadrado, suavizado, 
        % umbralización y búsqueda de picos en la señal. 
        derivada=filter(b,a,mi_senal); 
        cuadrado = derivada.^2;
        suavizada = filter(b4,a,cuadrado);
        [pos,Th] = detec_pic(suavizada',0.3,2);   % Pos =vector que contiene las pocisiones en la señal donde se encuentran los picos 
        r=mode(mi_senal);   %valor que mas se repite en cada señal 
        n=length(pos);
    
        %loop para sustituir los valores donde aparecen los picos por valores normales 
        for k=1:n
            if  n>1
                if(k<n)
                    if pos(k+1)>pos(k)+10
                        k=k+1; 
                    else
                        for m=pos(k):pos(k+1)
                            ecg(m,i)=r;
                        end
                    end
        
                else
                    k=k+1;   
                end     
            end
            figure(),subplot(2,1,1),plot(t,mi_senal),title('original'),subplot(2,1,2),plot(t,ecg(:,i)); title('Señal después de eliminar picos');
        end
    
        % PASO 2) Filtrado de paso bajo.
        y=lowpass(ecg(:,i),30,fs);
        figure,subplot(2,1,1),plot(ecg(:,i)),title('señal antes'),subplot(2,1,2),plot(y(30:29900)),title('Señal suavizada (filtro paso bajo)',i);
    
        
        % PASO 3) Filtrado de interferencias de línea eléctrica.
        
        [X,W]=freqz(y,1,2048);    
        f= (W*fs)/(2*pi);
        %figure,plot(f,abs(X)); title('espectro en magnitud',i);     % interferencia a frecuencia=50 Hz
    
        y2=filter(b2,a2,y);     %filtro notch, omega0=2*pi*50/1000=0.1*pi
        figure;plot(y2); title('Señal sin interferencias de linea electrica',i);
     
        %[X2,W2]=freqz(y2,1,2048);   
        %figure;plot(f,abs(X2));    % se ha eliminado la interferencia de la linea electrica
    
    
        % PASO 4) Desplazamiento de la línea base y eliminación de la compensación.
        filtrado_1=filter(b3,a,y2);  
        %figure,plot(filtrado_1(300:length(filtrado_1))); title('desplazamiento linea de base')     
        
        fc=8;     
        omegac=(2*pi*fc)/1000;
        [B,A]=butter(8,omegac/pi);  
        filtrado_2=filter(B,A,filtrado_1);  %eliminacion de la compensacion
        figure,plot(filtrado_2(900:length(filtrado_2))); title('Resultado final',i);
        
     end
   
    
    


    
end






