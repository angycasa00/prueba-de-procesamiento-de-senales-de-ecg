function plot_mult_curves2(t,x,separation)

[L,N]=size(x);
figure
plot(t,x(:,1)), grid
hold on
for i=2: N
    plot(t,(i-1)*separation + x(:,i))
end
